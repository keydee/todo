var ToDoItem = React.createClass({
    status: {
        NEW: 0,
        DONE: 1
    },

    getInitialState: function () {
        return {
            title: this.props.title,
            status: this.props.completed,
            description: this.props.children
        };
    },

    iterateStatus: function () {
        this.setState(
            {status: !this.state.status}
        );
    },

    updateText: function (event){
        this.setState({description: event.target.value});
    },

    render: function () {
        return(
            <div className={this.state.status?"completed":"new"}>
                <input type="checkbox" checked={this.state.status} onClick={this.iterateStatus}>{this.state.title}</input>
                <input type="text" value={this.state.description} onChange={this.updateText}></input>
            </div>
        );
    }
});