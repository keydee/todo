var ToDoItemPanel = React.createClass({
    getInitialState: function () {
        return {
            allToDos: [
                {
                    title: "demo title",
                    description: "desc",
                    status: 1
                },
                {
                    title: "demo title2",
                    description: "desc2",
                    status: 0
                },
                {
                    title: "demo title3",
                    description: "desc3",
                    status: 1
                },
                {
                    title: "demo title4",
                    description: "desc4",
                    status: 1
                }
            ]
        };
    },

    getListedTodos: function(){
        console.log(this.state.allToDos);
        return this.state.allToDos.map(function(singleItem){
            return (
                <div>
                    <ToDoItem title={singleItem.title} completed={singleItem.status}>{singleItem.description}</ToDoItem>
                </div>
            )
        });
    },

    addNote: function(event){
        this.state.allToDos.push({
            title: "new title",
            description: "new desc"
        });
        this.setState({
            allToDos: this.state.allToDos,
            });
    },

    render: function() {
        return (
            <div class="todoItemPanel">
                <div>{this.getListedTodos()}</div>
                <div>
                    You still have {this.state.allToDos.length} todos, but if there's
                    <button onClick={this.addNote}>Something more todo!</button>
                </div>
            </div>
        )
    }
});